package controllers

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

func HomeIndex(c *gin.Context) {
	// 注意下面将gin.H参数传入index.tmpl中!也就是使用的是index.tmpl模板
	c.HTML(http.StatusOK, "index.html", gin.H{
		"title": "GIN: 这是templates下的",
	})
}
