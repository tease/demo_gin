package dbaccess

import (
	"database/sql"
	"demo_gin/entities"
)

// GetDb 获取mysql数据库连接池
func GetDb() *sql.DB {
	db, _ := sql.Open("mysql", "数据库账户:密码@tcp(IP:端口)/数据库名?charset=utf8")
	db.SetMaxOpenConns(2000)
	db.SetMaxIdleConns(1000)
	db.Ping()
	return db
}

// GetUserInfo 根据id获取用户信息
func GetUserInfo(id string) *entities.UserInfo {
	db := GetDb()
	defer db.Close()
	row := db.QueryRow("SELECT id,username,passwordhash,locked FROM userinfo WHERE id=?", id)
	tuser := new(entities.UserInfo)
	row.Scan(&tuser.Id, &tuser.UserName, &tuser.Passwordhash, &tuser.Locked)
	return tuser
}
