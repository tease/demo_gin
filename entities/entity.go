package entities

//属性名第一个字母大写表示public
type UserInfo struct {
	Id           string `json:"id" form:"id"`
	UserName     string `json:"username" form:"username"`
	Passwordhash string `json:"passwordhash" form:"passwordhash"`
	Locked       bool   `json:"locked" form:"locked"`
}
