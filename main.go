package main

import (
	"net/http"

	"demo_gin/controllers"
	"demo_gin/dbaccess"

	"github.com/gin-gonic/gin"
	_ "github.com/go-sql-driver/mysql"
)

func main() {
	router := gin.Default()
	// 静态文件服务
	router.StaticFS("/content", http.Dir("content"))

	// 批量模板加载方式
	// router.LoadHTMLGlob("templates/**/*")

	// 指定文件模板加载方式
	router.LoadHTMLFiles(
		"templates/layout/header.html",
		"templates/layout/bottom.html",
		"templates/layout/footer.html",
		"templates/index.html",
		"templates/home/index.html")

	router.GET("/", controllers.HomeIndex)
	router.GET("/index", controllers.HomeIndex)
	router.GET("/home/index", func(c *gin.Context) {
		// 注意下面将gin.H参数传入index.tmpl中!也就是使用的是index.tmpl模板
		c.HTML(http.StatusOK, "home/index.html", gin.H{
			"title": "GIN: 这是templates/home下的",
		})
	})
	router.GET("/userinfo/:id", func(c *gin.Context) {
		id := c.Param("id")
		user := dbaccess.GetUserInfo(id)

		c.JSON(http.StatusOK, gin.H{
			"person": user,
		})
	})
	router.Run(":8080")
}
